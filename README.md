# Dynamic UK map

The goal was to represent equidistant points of sqrt (3) * 10 km in the UK on a dynamic map.

Check out map: http://nbviewer.jupyter.org/gist/AdeleH/9e79aa6bbf3775f659c83b90c51cf51c

![Alt text](/screenshots/dynamic_uk_map.png?raw=true "Dynamic UK Map using Folium")

![Alt text](/screenshots/zoomX.png?raw=true)

![Alt text](/screenshots/zoomY.png?raw=true)


Using Folium and Shapely

Hexagonal Packing Method (algorithm)

Folium documentation: https://python-visualization.github.io/folium/

Shapely: https://github.com/Toblerity/Shapely

I used Folium up to 70k markers.
